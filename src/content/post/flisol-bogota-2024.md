---
publishDate: 2024-05-04T00:00:00Z
title: 'FLiSoL Bogotá 2024.'
excerpt: Como parte de la programación de los 20 años de FLiSoL Bogotá 2024 dimos una charla y acompañamos otras dos charlas parte de la programación de HackBo en el festival.
image: '~/assets/images/ruydo-banner-horizontal-flisol-2024.png'
category: Ponencias
tags:
  - FLiSoL
  - HackBo
  - Charlas
metadata:
  canonical: https://ruydo.xyz/flisol-bogota-2024
---

_Herramientas y redes libres para hacer ruYdo_, fue el nombre de la charla que dimos en el FliSoL Bogotá 2024,
donde compartimos y explicamos las infraestructuras digitales, alianzas y redes con otros grupos y espacios para hacer:

* El canal de youtube [Ardi Complot](https://youtube.com/ardicomplot).
* El instrumento digital con el que [ruidajo](https://ruidajo.gitlab.io) hace música y ruido.
* Está página web y otros proyectos.

## Herramientas y redes libres para hacer ruYdo.

https://flisolbogota.org/2024/2024/04/23/744/

**CONFERENCISTAS**: Adriana Poveda y Diego Herrera García.

**COMUNIDAD**: ruYdo y [HackBo](https://hackbo.org)

![FLiSoL Bogotá 2024, banner horizontal del evento](~/assets/images/ruydo-banner-flisol-2024.jpg) 

**¿A QUIÉN VA DIRIGIDA LA CHARLA?**: 
Estudiantes, artistas, diseñadores, autodidactas o 
público interesado en la creación y producción de multimedia 
y gestión del conocimiento con herramientas digitales libres y abiertas.

**DURACIÓN**: 25 min.

**DESCRIPCIÓN DE LA CHARLA**: 
La producción audiovisual y multimedia requiere un ecosistema de tecnologías
para el manejo, organización, publicación y distribución de obras y productos
y una red de comunidades y agentes para compartir, distribuiur y apoyarnos mutuamente.
Desde ruYdo studio hemos explorado, creado y publicado obras y productos
llevando a la práctica las ideas de programación intersticial
interconectando herramientas libres para la producción de
instrumentos para la interpretación musical y la producción de videos sobre opinión y cultura músical.
En esta ocasión revisaremos el caso de las exploraciones de síntesis y procesamiento de sonido en vivo
usados en difernetes proyectos como en la banda de jazz, metal y noise bogotana MULA
y la escritura, producción y postproducción de audiovisual con el canal de youtube de Ardi Complot.
Mostraremos nuestro de uso de Kdenlive, Musescore, Inkscape, Gimp, Tiddlywiki, Puredata~,
Ardour, Pharo y herramientas propias para la creación y producción de obras y herramientas multimedia
y nuestra relación con las redes multidisciplinares y agentes cono HackBo, grafoscopio, mutabiT, FemHack y mutanteLab.

## HackBo e I.A. en educación

![Charlas de HackBo en el flisol Bogotá 2024](~/assets/images/hackbo-flisol-bogota-2024.png)

A parte de nuestra charla estuvimos acompañando a amiguis del hackerspace
con dos charlas:

* HackBo: un hackerspace en bogotá.
  * https://flisolbogota.org/2024/2024/04/23/739/
* IA en Educación, un enfoque crítico y prácticas locales.
  * https://flisolbogota.org/2024/2024/04/24/782/

Pueden consultar más info de esas charlas en: https://hackbo.org/posts/flisol-bogota-2024/