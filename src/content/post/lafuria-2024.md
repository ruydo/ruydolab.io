---
publishDate: 2024-05-18T00:00:00Z
title: 'Concierto y charlas en La Furia [del libro y la cultura] Anarquista 2024.'
excerpt: Dimos un par de charlas y un concierto en vivo con la compañía de Natalia Piza de FemHack en el marco de La Furia Anarquista 2024.
image: '~/assets/images/musiquita-anarca-furia-2024-banner-cuadrado.jpg'
category: Conciertos
tags:
  - Furia
  - Musica
  - Concierto
  - En vivo
  - Charla
  - Cuidados digitales
  - Anarquia
metadata:
  canonical: https://ruydo.xyz/lafuria-2024
draft: false 
---

# La Furia [del libro y la cultura] anaraquista

Feria del libro, encuentros, talleres entre otros varios eeventos culturales
en torno a la cultura anarquista.

En esta tercera edición se realizaron alrededor de 
30 eventos culturales y la feria del libro anarquista, 
con más de 40 parches, editoriales y proyectos 
que están pensando/creando rebeldías desde la autogestión.

![Poster de La Furia 2024](~/assets/images/la-furia-banner-vertical.jpg)

## Musiquita Anarca

Musiquita Anarca: un análisis imprevisto e impreciso de melodías sin autoridad.

Sábado 11 de mayo a las 4 p.m. [Ardi Complot](https://youtube.com/ardicomplot), junto a ruidajo
presentan una selección de música anarquista que analizarán desde una perspectiva musical.

Lugar: Prosa del Mundo - Cl 43 # 19 - 08.

Aquí pueden consultar la herramienta web en la que vamos registrando el proyecto:
https://ruidajo.gitlab.io/furia2024/musiquita-anarca

Evento en autonoma.red: https://autonoma.red/event/musiquita-anarca

![Musiquita Anarca](~/assets/images/musiquita-anarca-furia-2024-banner-cuadrado.jpg)

## Cuidados digitales y redes descentralizadas

Exploramos los datos (in)visibles que usan los oligopolios y sus plataformas de redes sociales 
para explotarnos y hacer lucro con nuestras interacciones 
y de ahi exploramos nuevas herramientas para gestionar nuestras propias interacciones.

Realizado en colaboración con [HackBo](https://hackbo.org/posts/cuidados-digitales-redes-descentralizadas/) 
y en La Redada Cll 17 # 02 - 51 eñ jueves 16 de Mayo 2024 a las 4 p.m.

Evento en autonoma.red: https://autonoma.red/event/cuidados-digitales-y-redes-descentralizadas

![Cuidados Digitales](~/assets/images/cuidados-digitales-lafuria-2024.webp)

## Concierto de Inauguración de la feria del libro

El viernes 17 de mayo fue la inauguración en la redada! Cll. 17 # 2 - 51. De 5 a 9 p.m.

Con la participación de Ianerre @olasobreola, con una lectura performática, 
ruYdo + la colectiva FemHack, improvisación musical 
y Aida Pamela @aida.esta.ida voz y guitarra.

Evento en autonoma.red: https://autonoma.red/event/inauguracion-feria-del-libro-la-furia

![Concierto inauguración de La Furia](~/assets/images/ruydo-banner-cuadrado-lafuria-2024.jpg)