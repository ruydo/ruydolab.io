---
publishDate: 2024-07-16T00:00:00Z
title: 'Entretejer Vibraciones Pronto para el día mundial de la escucha 2024, Escuchando el tejido del tiempo.'
excerpt: Invitamos y organizamos junto a HackBo a el evento Entretejer Vibraciones Pronto para la celebración del día mundial de la eschucha, donde nos encontramos a colaborar y compartir en torno a la escucha.
image: '~/assets/images/entretejer-vibraciones-pronto-wld-2024-cuadrado-banner.png'
category: Eventos
tags:
  - WLD
  - HackBo
  - Escucha
  - Día Mundial de la Escucha
metadata:
  canonical: https://ruydo.xyz/entretejer-vibraciones-pronto
draft: false
---

Les invitamos a encontrarnos por un par de horas en HackBo,
para crear, conversar y reflexionar en torno a la escucha.

Este evento se conecta con las experiencias y la memoria
de los eventos realizados en HackBo y con la comunidad de Grafoscopio 
en 2021 "[Inquieta y ruidosa](https://docutopia.sustrato.red/s/dataroda61)" y en 2022 "[Orejas sin límites](https://docutopia.sustrato.red/s/dataroda70)".

En esta ocación proponemos un espacio para cocrear sonoridades,
conversar y reflexionar en como a cambiado nuestro entorno sonoro
y como estos cambios entretejen nuestras relaciones con los demas, con los espacios,
con objetos, con otras criaturas vivas y no-vivas y con nosotros mismos.

- Lugar: HackBo, Calle 44 # 8 - 50. Of. 201.
- Fecha: Jueves 18 de Julio, 6PM a 8PM.
- **Inscripciones**: https://is.gd/wld2024

Puedes agregar este evento en tu calendario visitando: https://autonoma.red/event/entretejer-vibraciones-pronto-or-dia-mundial-de-la-escucha-2024

Enlaces adicionales:

- https://hackbo.org/posts/wld-2024/
- https://worldlisteningday.org/events/entretejer-vibraciones-pronto-ruydo-hackbo/

![Entretejer Vibraciones Pronto. poster](~/assets/images/entretejer-vibraciones-pronto-wld-2024-poster.png) 
