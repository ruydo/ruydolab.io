# 🪅 ruYdo

Somos una agrupación de investigación, creación e innovación sonora y audiovisual. :)

Esta es nuestra web que esta en constante (des/cons)trucción. https://ruydo.xyz

Construida a partir de [AstroWind](https://github.com/onwidget/astrowind) ↙

# 🚀 AstroWind

**AstroWind** is a free and open-source template to make your website using **[Astro 4.0](https://astro.build/) + [Tailwind CSS](https://tailwindcss.com/)**. Ready to start a new project and designed taking into account web best practices.

## Demo

📌 [https://astrowind.vercel.app/](https://astrowind.vercel.app/)

## Acknowledgements

Initially created by [onWidget](https://onwidget.com) and maintained by a community of [contributors](https://github.com/onwidget/astrowind/graphs/contributors).

## License

**AstroWind** is licensed under the MIT license — see the [LICENSE](./LICENSE.md) file for details.
